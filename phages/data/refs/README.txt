Streptococcus thermophilus

Sequences downloaded from:
https://www.ncbi.nlm.nih.gov/genome/?term=txid1268061[Organism:noexp]

Files downloaded:
GCF_000253395.1_ASM25339v1_genomic.fna.gz
GCF_000253395.1_ASM25339v1_genomic.gff.gz
GCF_000253395.1_ASM25339v1_genomic.gbff.gz
GCF_000253395.1_ASM25339v1_protein.faa.gz

Date downloaded: 
20190524

