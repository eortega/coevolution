Mapping statistics:

Percentage andnumber of reads mapped per sample in file `mapping_flagstat.txt`, created like this
```bash
cd mapping
for i in $(find . -name *.bam); 
do 
    echo -ne $i'\t'$(samtools flagstat -@ 10 $i | grep 'mapped (')'\n'  >> mapping_flagstat.txt; 
done
```
