Steps followed

* qual/
    the first step was to check the quality of the raw data

* trimmed/
    Contains cleaned and trimmed data to be used in the following steps

* mapping/
    Contains the .sam .sorted.bam and .bai produced after mapping

* snpcalling/
    .vcf coming from freebayes
