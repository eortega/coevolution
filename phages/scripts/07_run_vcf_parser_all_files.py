#! /home/enrique/envs/biopython/bin/python

import vcf_parser2



# sample_files = [path + "R2T1_S28_phag.vcf", path + "R2T2_S44_phag.vcf", path +"R2T3_S60_phag.vcf", path + "R2T4_S76_phag.vcf"]

# outpath = '/mnt/alpha_raid/work/Gandon/phages/test_freebayes/plots/binary_plots/'

# r1t1 = vcf_parser2.ImportVCF()
# r1t1.load_vcf(sample_files[0])

# r1t2 = vcf_parser2.ImportVCF()
# r1t2.load_vcf(sample_files[1])

# r1t3 = vcf_parser2.ImportVCF()
# r1t3.load_vcf(sample_files[2])

# r1t4 = vcf_parser2.ImportVCF()
# r1t4.load_vcf(sample_files[3])

# samples_indexes_list = [r1t1.vcf_index, r1t2.vcf_index, r1t3.vcf_index, r1t4.vcf_index]
# headers = ['T1', 'T2', 'T3', 'T4']
# patate = vcf_parser2.Patate(samples_indexes_list, headers, control.ctrl_index)
# # print(patate.binary_df)
# patate.plot_binary_matrix(outpath + 'bin_plot_R2' + ".png", "R2")



def run_on_multiple_files(path, outpath, list_files, list_headers, group_name, control_list):
    '''
    Paths must end with a "/"
    len(list_files) == len(list_headers)
    '''
    samples_indexes_list = []
    for fichier in list_files :
        print( path + fichier )
        temp = vcf_parser2.ImportVCF()
        temp.load_vcf(path + fichier)
        samples_indexes_list.append(temp.vcf_index)
    the_plot = vcf_parser2.Patate(samples_indexes_list, list_headers, control_list)
    the_plot.plot_binary_matrix(outpath + 'bin_plot_' + group_name + '.png', group_name)
    # return the_plot


####################################################################################
####################################################################################
####################################################################################
####################################################################################

####
##  DECLARE PATHS
####

ctrl_file = '/mnt/alpha_raid/work/Gandon/phages/test_freebayes/results/TO-WT_S83_phag.vcf'

path = "/mnt/alpha_raid/work/Gandon/phages/test_freebayes/results/"
outpath = '/mnt/alpha_raid/work/Gandon/phages/test_freebayes/plots/binary_plots/'


####
##  LOAD CONTROL
####

control = vcf_parser2.ImportControl()
control.load_vcf(ctrl_file)
## control.ctrl_index  ## That is the list of indexes of the control



####
##  LIST OF FILES PER GROUP
####

r1 = ['R1T1_S27_phag.vcf', 'R1T2_S43_phag.vcf', 'R1T3_S59_phag.vcf', 'R1T4_S75_phag.vcf']
r2 = ['R2T1_S28_phag.vcf', 'R2T2_S44_phag.vcf', 'R2T3_S60_phag.vcf', 'R2T4_S76_phag.vcf']
r3 = ['R3T1_S29_phag.vcf', 'R3T2_S45_phag.vcf', 'R3T3_S61_phag.vcf', 'R3T4_S77_phag.vcf']
r4 = ['R4T1_S30_phag.vcf', 'R4T2_S46_phag.vcf', 'R4T2-B_S88_phag.vcf', 'R4T3_S62_phag.vcf', 'R4T4_S78_phag.vcf']
r5 = ['R5T1_S31_phag.vcf', 'R5T2_S47_phag.vcf', 'R5T3_S63_phag.vcf', 'R5T4_S79_phag.vcf']
r6 = ['R6T1_S32_phag.vcf', 'R6T2_S48_phag.vcf', 'R6T3_S64_phag.vcf', 'R6T4_S80_phag.vcf']
r7 = ['R7R1_S33_phag.vcf', 'R7R2_S49_phag.vcf', 'R7T2-B_S89_phag.vcf', 'R7R3_S65_phag.vcf', 'R7R4_S81_phag.vcf']
r8 = ['R8T1_S34_phag.vcf', 'R8T2_S50_phag.vcf', 'R8T3_S66_phag.vcf', 'R8T4_S82_phag.vcf']


####
##  LIST OF HEADERS (BY COLUMNS). THIS ARE THE LABELS ON THE X AXIS OF THE PLOT
####

std_headers = ['T1', 'T2', 'T3', 'T4']

r1_headers = std_headers
r2_headers = std_headers
r3_headers = std_headers
r4_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']
r5_headers = std_headers
r6_headers = std_headers
r7_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']
r8_headers = std_headers



####
##  ZHU LI DO THE THING
####

run_on_multiple_files(path, outpath, r1, r1_headers, "R1", control.ctrl_index)
run_on_multiple_files(path, outpath, r2, r2_headers, "R2", control.ctrl_index)
run_on_multiple_files(path, outpath, r3, r3_headers, "R3", control.ctrl_index)
run_on_multiple_files(path, outpath, r4, r4_headers, "R4", control.ctrl_index)
run_on_multiple_files(path, outpath, r5, r5_headers, "R5", control.ctrl_index)
run_on_multiple_files(path, outpath, r6, r6_headers, "R6", control.ctrl_index)
run_on_multiple_files(path, outpath, r7, r7_headers, "R7", control.ctrl_index)
run_on_multiple_files(path, outpath, r8, r8_headers, "R8", control.ctrl_index)




####################################################################################
####################################################################################
####################################################################################
####################################################################################

####
##  LIST OF FILES PER GROUP
####

w1 =['W1T1_S18_phag.vcf', 'W1T2_S35_phag.vcf', 'W1T3_S51_phag.vcf', 'W1T4_S67_phag.vcf']
w2 =['W2T1_S19_phag.vcf', 'W2T2_S36_phag.vcf', 'W2T2-B_S85_phag.vcf', 'W2T3_S52_phag.vcf', 'W2T4_S68_phag.vcf']

w3 =['W3T1_S20_phag.vcf', 'W3T1-bis_S95_phag.vcf', 'W3T2_S37_phag.vcf', 'W3T3_S53_phag.vcf', 'W3T4_S69_phag.vcf', 'W3T4-B_S90_phag.vcf']
w4 =['W4T1_S21_phag.vcf', 'W4T2_S38_phag.vcf', 'W4T2-B_S86_phag.vcf', 'W4T3_S54_phag.vcf', 'W4T4_S70_phag.vcf']

w5 =['W5T1_S22_phag.vcf', 'W5T2_S39_phag.vcf', 'W5T2-B_S87_phag.vcf', 'W5T3_S55_phag.vcf', 'W5T4_S71_phag.vcf']
w6 =['W6T1_S23_phag.vcf', 'W6T2_S40_phag.vcf', 'W6T3_S56_phag.vcf', 'W6T4_S72_phag.vcf']

w7 =['W7T1_S24_phag.vcf', 'W7T1-B_S94_phag.vcf', 'W7T2_S41_phag.vcf', 'W7T3_S57_phag.vcf', 'W7T4_S73_phag.vcf', 'W7T4-B_S91_phag.vcf']
w8 =['W8T1_S26_phag.vcf', 'W8T2_S42_phag.vcf', 'W8T3_S58_phag.vcf', 'W8T4_S74_phag.vcf', 'W8T4-B_S92_phag.vcf']


####
##  LIST OF HEADERS (BY COLUMNS). THIS ARE THE LABELS ON THE X AXIS OF THE PLOT
####

std_headers = ['T1', 'T2', 'T3', 'T4']

w1_headers = std_headers
w2_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']

w3_headers = ['T1', 'Tb1', 'T2', 'T3', 'T4', 'T4b']
w4_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']

w5_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']
w6_headers = std_headers

w7_headers = ['T1', 'Tb1', 'T2', 'T3', 'T4', 'T4b']
w8_headers = ['T1', 'T2', 'T3', 'T4', 'T4b']


####
##  ZHU LI DO THE THING
####

run_on_multiple_files(path, outpath, w1, w1_headers, "W1", control.ctrl_index)
run_on_multiple_files(path, outpath, w2, w2_headers, "W2", control.ctrl_index)
run_on_multiple_files(path, outpath, w3, w3_headers, "W3", control.ctrl_index)
run_on_multiple_files(path, outpath, w4, w4_headers, "W4", control.ctrl_index)
run_on_multiple_files(path, outpath, w5, w5_headers, "W5", control.ctrl_index)
run_on_multiple_files(path, outpath, w6, w6_headers, "W6", control.ctrl_index)
run_on_multiple_files(path, outpath, w7, w7_headers, "W7", control.ctrl_index)
run_on_multiple_files(path, outpath, w8, w8_headers, "W8", control.ctrl_index)
