#!/bin/bash


###################################
# VARIABLE CREATION
## Create the output directory

## Arguments in the order
##      1- subject fasta (Reference for blast)
##      2- query fasta (sequences to be researched against the reference)
##      3- step name (output directory into steps/)
##      4- number of threads
##      5- blastDB name and directory


# subject_fasta=steps/PAM_detection/PAM_protospacers.fasta
# query_fasta=data/bim/bim_old_names.fasta
# outdir=steps/blast_old_bim/

subject_fasta=$1
query_fasta=$2
outdir=$3
threads=$4
blast_db_name=$5
blast_db_dir=$(dirname $blast_db_name)


# blast_db_dir=data/blastDB/
# blast_db_name=data/blastDB/PAM_protospacers


###################################

## Create directories for output and blastDB
mkdir -p $outdir
mkdir -p $blast_db_dir


# sed 's/ .*$//' $subject_fasta > ${subject_fasta/.fasta/_2.fasta} 
phages $ makeblastdb  -in ${subject_fasta}  -dbtype nucl -out $blast_db_name

##  Create a database
makeblastdb  -in ${subject_fasta}  \
	-dbtype nucl  -out $blast_db_name  -parse_seqids  -title PAMs


## Blast stuff
blastn  -query $query_fasta \
	-out blast_St.out6 \
	-subject ${subject_fasta} \
    -word_size 15 \
	-outfmt 6

    # -num_threads $threads
