#! /home/enrique/envs/biopython/bin/python

from vcf_parser3 import *


def run_on_multiple_files(path_to_vcfs, outpath, list_files, list_headers, group_name, control_list, file_root):
    
    samples_indexes_list = []
    for fichier in list_files :
            # print( path_to_vcfs + fichier )
            temp = ImportVCF2()
            temp.load_vcf(path_to_vcfs + fichier)
            samples_indexes_list.append(temp.index)
            the_plot = FrequencyOneSNP(samples_indexes_list, list_headers, control_list, temp, 'snp', outpath, file_root)
            the_plot.plot_frequency_matrix(outpath + 'freq_plot_' + group_name + '.png', group_name)
    return the_plot




####
##  LOAD CONTROL
####
ctrl_file = '/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/Other_seq/TO-WT_S83.vcf'

control = ImportControl2()

control.load_vcf(ctrl_file)




####
##  DECLARE PATHS for W sequences
####

path_to_vcfs = '/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/W_seq/'

# outpath = '/home/enrique/work/Gandon/coevolution/phages/plots/freq_heatmap/W_seq/'
outpath = '/home/enrique/work/Gandon/coevolution/phages/debug_freq'


w1 =['W1T1_S18.vcf', 'W1T2_S35.vcf', 'W1T3_S51.vcf', 'W1T4_S67.vcf']



####
##  LIST OF HEADERS (BY COLUMNS). THIS ARE THE LABELS ON THE X AXIS OF THE PLOT
####

std_headers = ['T1', 'T2', 'T3', 'T4']

w1_headers = std_headers


foo = run_on_multiple_files(path_to_vcfs, outpath, w1, w1_headers, "W1", control.index, 'w1_table')



