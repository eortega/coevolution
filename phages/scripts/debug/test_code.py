from vcf_parser3 import *


def run_on_multiple_files(path_to_vcfs, outpath, list_files, list_headers, group_name, control_list, file_root):
    
    samples_indexes_list = []
    for fichier in list_files :
            # print( path_to_vcfs + fichier )
            temp = ImportVCF2()
            temp.load_vcf(path_to_vcfs + fichier)
            samples_indexes_list.append(temp.index)
            the_plot = FrequencyOneSNP(samples_indexes_list, list_headers, control_list, temp, 'snp', outpath, file_root)
            the_plot.plot_frequency_matrix(outpath + 'freq_plot_' + group_name + '.png', group_name)
    return the_plot


##################################################


####
##  LOAD CONTROL
####
ctrl_file = '/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/Other_seq/TO-WT_S83.vcf'

control = ImportControl2()

control.load_vcf(ctrl_file)


##################################################


####
##  DECLARE PATHS for W sequences
####

path_to_vcfs = '/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/W_seq/'

outpath = '/home/enrique/work/Gandon/coevolution/phages/scripts/debug/'


####
##  LIST OF FILES PER GROUP
####

w1 =['W1T1_S18.vcf', 'W1T2_S35.vcf', 'W1T3_S51.vcf', 'W1T4_S67.vcf']

w2 =['W2T1_S19.vcf', 'W2T2_S36.vcf', 'W2T3_S52.vcf', 'W2T4_S68.vcf']

w3 =['W3T1_S20.vcf', 'W3T2_S37.vcf', 'W3T3_S53.vcf', 'W3T4_S69.vcf']

w4 =['W4T1_S21.vcf', 'W4T2_S38.vcf', 'W4T3_S54.vcf', 'W4T4_S70.vcf']

w5 =['W5T1_S22.vcf', 'W5T2_S39.vcf', 'W5T3_S55.vcf', 'W5T4_S71.vcf']

w6 =['W6T1_S23.vcf', 'W6T2_S40.vcf', 'W6T3_S56.vcf', 'W6T4_S72.vcf']

w7 =['W7T1_S24.vcf', 'W7T2_S41.vcf', 'W7T3_S57.vcf', 'W7T4_S73.vcf']

w8 =['W8T1_S26.vcf', 'W8T2_S42.vcf', 'W8T3_S58.vcf', 'W8T4_S74.vcf']


####
##  LIST OF HEADERS (BY COLUMNS). THIS ARE THE LABELS ON THE X AXIS OF THE PLOT
####

std_headers = ['T1', 'T2', 'T3', 'T4']

w1_headers = std_headers

w2_headers = std_headers

w3_headers = std_headers

w4_headers = std_headers

w5_headers = std_headers

w6_headers = std_headers

w7_headers =std_headers

w8_headers = std_headers


####
##  ZHU LI DO THE THING
####


foo = run_on_multiple_files(path_to_vcfs, outpath, w1, w1_headers, "W1", control.index, 'w1_table')
# foo = run_on_multiple_files(path_to_vcfs, outpath, w2, w2_headers, "W2", control.index, 'w2_table')
# foo = run_on_multiple_files(path_to_vcfs, outpath, w3, w3_headers, "W3", control.index, 'w3_table')
# foo = run_on_multiple_files(path_to_vcfs, outpath, w4, w4_headers, "W4", control.index, 'w4_table')
# foo = run_on_multiple_files(path_to_vcfs, outpath, w5, w5_headers, "W5", control.index, 'w5_table')
# foo = run_on_multiple_files(path_to_vcfs, outpath, w6, w6_headers, "W6", control.index, 'w6_table')
# foo = run_on_multiple_files(path_to_vcfs, outpath, w7, w7_headers, "W7", control.index, 'w7_table')
# foo = run_on_multiple_files(path_to_vcfs, outpath, w8, w8_headers, "W8", control.index, 'w8_table')
