#! /home/enrique/envs/biopython/bin/python

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

t0 = ["NC_007019.1_2","NC_007019.1_4"]          # list to be removed (control)
# List of indexes from samples
t1 = ["NC_007019.1_1","NC_007019.1_3","NC_007019.1_5","NC_007019.1_6","NC_007019.1_7"]
t2 = ["NC_007019.1_2","NC_007019.1_3","NC_007019.1_4","NC_007019.1_6","NC_007019.1_8"]
t3 = ["NC_007019.1_1","NC_007019.1_4","NC_007019.1_5","NC_007019.1_7","NC_007019.1_9"]
t4 = ["NC_007019.1_4","NC_007019.1_5","NC_007019.1_7","NC_007019.1_8","NC_007019.1_9"]

# Put all lists together and sort them
t1234 = list(set(sorted(t1 + t2 + t3 + t4)))
t1234.sort()

# Create a new list to store the "control free" data
newt1234=[]

# Fill the new list with indexes which are not in the control
for i in t1234:
    if i not in t0:
        newt1234.append(i)


# newt1234

# Create new lists for all samples
t1b, t2b, t3b, t4b = [], [], [], []

# Fill lists with binary "presence/absence"
for i in newt1234:

    if i in t1:
        t1b.append(1)
    else:
        t1b.append(0)

    if i in t2:
        t2b.append(1)
    else:
        t2b.append(0)

    if i in t3:
        t3b.append(1)
    else:
        t3b.append(0)

    if i in t4:
        t4b.append(1)
    else:
        t4b.append(0)



# Create a pandas dataframe
mydf = pd.DataFrame(index = newt1234, data=({'T1':t1b, 'T2':t2b, 'T3':t3b, 'T4':t4b}))


# Create pandas dataframe from a dictionary:
newt1234_dico={}
headers = ['T1', 'T2', 'T3', 'T4']
list_of_lists = [t1b, t2b, t3b, t4b]
for i, j in zip(headers, list_of_lists):
    newt1234_dico[i] = j

mydf2 = pd.DataFrame(index = newt1234, data = newt1234_dico)


#################
## MAKE FIGURE
#################

## Creates an empty figure
fig = plt.figure()
## fig.suptitle("Presence/Absence of mutations")   # Figure title, useful in case of multi-ple graphic figures

## Creates the proportions the graphic will have (left, bottom, width, height)
ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])

## Title for that graphic
ax.set_title('Presence/Absence of mutations')

## Set x axis label and ticks and tick labels
ax.set_xlabel('Time')
ax.set_xticks([0, 1, 2, 3])
ax.set_xticklabels(['T1', 'T2', 'T3', 'T4'])

## Set y axis label and tick labels
ax.set_ylabel('Coordinate by experiment')
ax.set_yticklabels([0]+newt1234)

## Do the graph
plt.imshow(mydf
    , cmap='binary'     ## Color MAP
    # , interpolation='nearest'
    )

## Show the graph -- for dev and debug
plt.show()

## Save file to file -- for script
# fig.savefig('/mnt/alpha_raid/work/Gandon/phages/scripts/lib/toy_example.png')
# plt.close(fig)




