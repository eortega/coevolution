#! /home/enrique/envs/biopython/bin/python
import sys
import pandas as pd

## FIRST ARGUMENT MUST BE A BLAST RESULT FILE IN "outfmt 6" 
## SECOND ARGUMENT MUST BE A VCF FILE


# blast_file='/mnt/alpha_raid/work/Gandon/phages/results/blast_protospacers_30/blast_proto30-genome.tab'
blast_file = sys.argv[1]
# vcf_file='/mnt/alpha_raid/work/Gandon/phages/test_freebayes/results/TO-WT_S83_phag.vcf'
vcf_file=sys.argv[2]

with open(blast_file, 'r')  as sf :
	blast_lines = sf.readlines()

# coordinates_list = []

# for i in lines:
# 	line=i.split('\t')
# 	coordinates_list.append(line[8:10])  


with open(vcf_file, 'r')  as sf :
	vcf_lines = sf.readlines()

header=[]
vcf_contents=[]
for i in vcf_lines:
	if i[0] == '#':
		header.append(i)
	else:
		vcf_contents.append(i)



coordinates_list_altb = []

with open("sorted_blast_proto30-genome.tab", 'w') as df:
	for i in blast_lines:
		line = i.split('\t')
		# print(line)
		if int(line[8]) <= int(line[9]):
			coordinates_list_altb.append(line)
			df.write('\t'.join(line))
		else:
			coordinates_list_altb.append([line[0], line[1], line[2], line[3], line[4], line[5]\
				, line[6], line[7], line[9], line[8], line[10], line[11]])
			df.write('\t'.join([line[0], line[1], line[2], line[3], line[4], line[5]\
				, line[6], line[7], line[9], line[8], line[10], line[11]]))


datf = pd.DataFrame(coordinates_list_altb)

# datf[3] = datf[3].apply(float)
# datf[4] = datf[4].apply(int)
# datf[5] = datf[5].apply(int)
# datf[6] = datf[6].apply(int)
# datf[7] = datf[7].apply(int)
datf[8] = datf[8].apply(int)
datf[9] = datf[9].apply(int)
# datf[10] = datf[10].apply(float)
# datf[11] = datf[11].apply(float)

datf = datf.sort_values(8)
datf_ix = datf.reset_index()


####


mutation_in_region = []

count_mutations_in_protospacers = 0
# protospacers_containing_mutations

for i in vcf_contents:
	line = i.split('\t')
	vcf_pos = int(line[1])
	# print(line[1], type(line[1]))
	for j in range(0, len(datf_ix[0])):
		# print(datf_ix.loc[j])
		if vcf_pos in range(datf_ix.loc[j][8], datf_ix.loc[j][9]):
			#print(line, j)
			mutation_in_region.append(line)
			count_mutations_in_protospacers += 1
			# protospacers_containing_mutations.append()

print('{0}\t{1}'.format(vcf_file.split('/')[-1] \
	, count_mutations_in_protospacers))


## RUNNING EXAMPLES
## SINGLE RUN IN IPYTHON
# %run lib/blast_a_lt_b.py /mnt/alpha_raid/work/Gandon/phages/results/blast_protospacers_30/blast_proto30-genome.tab /mnt/alpha_raid/work/Gandon/phages/test_freebayes/results/TO-WT_S83_phag.vcf

# ## LOOP RUN IN BASH
# blast_result=/mnt/alpha_raid/work/Gandon/phages/results/blast_protospacers_30/blast_proto30-genome.tab
# result_file=/mnt/alpha_raid/work/Gandon/phages/results/intersect_blast_proto30_x_vcf.tab
# for i in /mnt/alpha_raid/work/Gandon/phages/test_freebayes/results/*; do python  /mnt/alpha_raid/work/Gandon/phages/scripts/lib/blast_a_lt_b.py /mnt/alpha_raid/work/Gandon/phages/results/blast_protospacers_30/blast_proto30-genome.tab $i | tee -a $result_file; done
