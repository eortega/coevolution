#! /home/enrique/envs/biopython/bin/python

from vcf_parser3 import *


#########################################################
### TEST À METTRE DANS LA LIBRAIRIE


# sample_files = [path + "R2T1_S28_phag.vcf", path + "R2T2_S44_phag.vcf", path +"R2T3_S60_phag.vcf", path + "R2T4_S76_phag.vcf"]

# outpath = '/mnt/alpha_raid/work/Gandon/phages/test_freebayes/plots/binary_plots/'

# r1t1 = ImportVCF()
# r1t1.load_vcf(sample_files[0])

# r1t2 = ImportVCF()
# r1t2.load_vcf(sample_files[1])

# r1t3 = ImportVCF()
# r1t3.load_vcf(sample_files[2])

# r1t4 = ImportVCF()
# r1t4.load_vcf(sample_files[3])

# samples_indexes_list = [r1t1.vcf_index, r1t2.vcf_index, r1t3.vcf_index, r1t4.vcf_index]
# headers = ['T1', 'T2', 'T3', 'T4']
# patate = Patate(samples_indexes_list, headers, control.ctrl_index)
# # print(patate.binary_df)
# patate.plot_binary_matrix(outpath + 'bin_plot_R2' + ".png", "R2")

#########################################################




def run_on_multiple_files(path_to_vcfs, outpath, list_files, list_headers, group_name, control_list, file_root):
    
    samples_indexes_list = []
    for fichier in list_files :
            # print( path_to_vcfs + fichier )
            temp = ImportVCF2()
            temp.load_vcf(path_to_vcfs + fichier)
            samples_indexes_list.append(temp.index)
            the_plot = FrequencyOneSNP(samples_indexes_list, list_headers, control_list, temp, 'snp', outpath, file_root)
            the_plot.plot_frequency_matrix(outpath + 'freq_plot_' + group_name + '.png', group_name)
    return the_plot



##################################################


####
##  LOAD CONTROL
####
ctrl_file = '/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/Other_seq/TO-WT_S83.vcf'

control = ImportControl2()

control.load_vcf(ctrl_file)


##################################################


####
##  DECLARE PATHS for W sequences
####

path_to_vcfs = '/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/W_seq/'

outpath = '/home/enrique/work/Gandon/coevolution/phages/plots/freq_heatmap/W_seq/'


####
##  LIST OF FILES PER GROUP
####

w1 =['W1T1_S18.vcf', 'W1T2_S35.vcf', 'W1T3_S51.vcf', 'W1T4_S67.vcf']
# w2 =['W2T1_S19.vcf', 'W2T2_S36.vcf', 'W2T2-B_S85.vcf', 'W2T3_S52.vcf', 'W2T4_S68.vcf']
w2 =['W2T1_S19.vcf', 'W2T2_S36.vcf', 'W2T3_S52.vcf', 'W2T4_S68.vcf']
# w3 =['W3T1_S20.vcf', 'W3T1-bis_S95.vcf', 'W3T2_S37.vcf', 'W3T3_S53.vcf', 'W3T4_S69.vcf', 'W3T4-B_S90.vcf']
w3 =['W3T1_S20.vcf', 'W3T2_S37.vcf', 'W3T3_S53.vcf', 'W3T4_S69.vcf']
# w4 =['W4T1_S21.vcf', 'W4T2_S38.vcf', 'W4T2-B_S86.vcf', 'W4T3_S54.vcf', 'W4T4_S70.vcf']
w4 =['W4T1_S21.vcf', 'W4T2_S38.vcf', 'W4T3_S54.vcf', 'W4T4_S70.vcf']
# w5 =['W5T1_S22.vcf', 'W5T2_S39.vcf', 'W5T2-B_S87.vcf', 'W5T3_S55.vcf', 'W5T4_S71.vcf']
w5 =['W5T1_S22.vcf', 'W5T2_S39.vcf', 'W5T3_S55.vcf', 'W5T4_S71.vcf']
w6 =['W6T1_S23.vcf', 'W6T2_S40.vcf', 'W6T3_S56.vcf', 'W6T4_S72.vcf']

# w7 =['W7T1_S24.vcf', 'W7T1-B_S94.vcf', 'W7T2_S41.vcf', 'W7T3_S57.vcf', 'W7T4_S73.vcf', 'W7T4-B_S91.vcf']
w7 =['W7T1_S24.vcf', 'W7T2_S41.vcf', 'W7T3_S57.vcf', 'W7T4_S73.vcf']
# w8 =['W8T1_S26.vcf', 'W8T2_S42.vcf', 'W8T3_S58.vcf', 'W8T4_S74.vcf', 'W8T4-B_S92.vcf']
w8 =['W8T1_S26.vcf', 'W8T2_S42.vcf', 'W8T3_S58.vcf', 'W8T4_S74.vcf']


####
##  LIST OF HEADERS (BY COLUMNS). THIS ARE THE LABELS ON THE X AXIS OF THE PLOT
####

std_headers = ['T1', 'T2', 'T3', 'T4']

w1_headers = std_headers
# w2_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']
w2_headers = std_headers

# w3_headers = ['T1', 'Tb1', 'T2', 'T3', 'T4', 'T4b']
w3_headers = std_headers
# w4_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']
w4_headers = std_headers

# w5_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']
w5_headers = std_headers
w6_headers = std_headers

# w7_headers = ['T1', 'Tb1', 'T2', 'T3', 'T4', 'T4b']
w7_headers =std_headers
# w8_headers = ['T1', 'T2', 'T3', 'T4', 'T4b']
w8_headers = std_headers


####
##  ZHU LI DO THE THING
####


foo = run_on_multiple_files(path_to_vcfs, outpath, w1, w1_headers, "W1", control.index, 'w1_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, w2, w2_headers, "W2", control.index, 'w2_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, w3, w3_headers, "W3", control.index, 'w3_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, w4, w4_headers, "W4", control.index, 'w4_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, w5, w5_headers, "W5", control.index, 'w5_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, w6, w6_headers, "W6", control.index, 'w6_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, w7, w7_headers, "W7", control.index, 'w7_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, w8, w8_headers, "W8", control.index, 'w8_table')


# run_on_multiple_files(path_to_vcfs, outpath, w1, w1_headers, "W1", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, w2, w2_headers, "W2", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, w3, w3_headers, "W3", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, w4, w4_headers, "W4", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, w5, w5_headers, "W5", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, w6, w6_headers, "W6", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, w7, w7_headers, "W7", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, w8, w8_headers, "W8", control.ctrl_index)





####################################################################################
####################################################################################
####################################################################################
####################################################################################

####
##  DECLARE PATHS for R sequences
####

path_to_vcfs = '/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/R_seq/'

outpath = '/home/enrique/work/Gandon/coevolution/phages/plots/freq_heatmap/R_seq/'


####
##  LIST OF FILES PER GROUP
####

r1 = ['R1T1_S27.vcf', 'R1T2_S43.vcf', 'R1T3_S59.vcf', 'R1T4_S75.vcf']
r2 = ['R2T1_S28.vcf', 'R2T2_S44.vcf', 'R2T3_S60.vcf', 'R2T4_S76.vcf']
r3 = ['R3T1_S29.vcf', 'R3T2_S45.vcf', 'R3T3_S61.vcf', 'R3T4_S77.vcf']
# r4 = ['R4T1_S30.vcf', 'R4T2_S46.vcf', 'R4T2-B_S88.vcf', 'R4T3_S62.vcf', 'R4T4_S78.vcf']
r4 = ['R4T1_S30.vcf', 'R4T2_S46.vcf', 'R4T3_S62.vcf', 'R4T4_S78.vcf']
r5 = ['R5T1_S31.vcf', 'R5T2_S47.vcf', 'R5T3_S63.vcf', 'R5T4_S79.vcf']
r6 = ['R6T1_S32.vcf', 'R6T2_S48.vcf', 'R6T3_S64.vcf', 'R6T4_S80.vcf']
# r7 = ['R7R1_S33.vcf', 'R7R2_S49.vcf', 'R7T2-B_S89.vcf', 'R7R3_S65.vcf', 'R7R4_S81.vcf']
r7 = ['R7R1_S33.vcf', 'R7R2_S49.vcf', 'R7R3_S65.vcf', 'R7R4_S81.vcf']
r8 = ['R8T1_S34.vcf', 'R8T2_S50.vcf', 'R8T3_S66.vcf', 'R8T4_S82.vcf']


####
##  LIST OF HEADERS (BY COLUMNS). THIS ARE THE LABELS ON THE X AXIS OF THE PLOT
####

std_headers = ['T1', 'T2', 'T3', 'T4']

r1_headers = std_headers
r2_headers = std_headers
r3_headers = std_headers
# r4_headers = ['T1', 'T2', 'T2b' 'T3', 'T4']
r4_headers = std_headers
r5_headers = std_headers
r6_headers = std_headers
# r7_headers = ['T1', 'T2', 'Tb2', 'T3', 'T4']
r7_headers = std_headers
r8_headers = std_headers



####
##  ZHU LI DO THE THING
####


foo = run_on_multiple_files(path_to_vcfs, outpath, r1, r1_headers, "R1", control.index, 'r1_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, r2, r2_headers, "R2", control.index, 'r2_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, r3, r3_headers, "R3", control.index, 'r3_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, r4, r4_headers, "R4", control.index, 'r4_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, r5, r5_headers, "R5", control.index, 'r5_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, r6, r6_headers, "R6", control.index, 'r6_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, r7, r7_headers, "R7", control.index, 'r7_table')
foo = run_on_multiple_files(path_to_vcfs, outpath, r8, r8_headers, "R8", control.index, 'r8_table')


# run_on_multiple_files(path_to_vcfs, outpath, r1, r1_headers, "R1", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, r2, r2_headers, "R2", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, r3, r3_headers, "R3", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, r4, r4_headers, "R4", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, r5, r5_headers, "R5", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, r6, r6_headers, "R6", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, r7, r7_headers, "R7", control.ctrl_index)
# run_on_multiple_files(path_to_vcfs, outpath, r8, r8_headers, "R8", control.ctrl_index)
