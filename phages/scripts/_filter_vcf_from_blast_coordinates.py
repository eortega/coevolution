#! /home/enrique/envs/biopython/bin/python
import sys

## First argument must be a blast result file in "outfmt 6" 
## Second Argument Must Be A Vcf File


# blast_file='/mnt/alpha_raid/work/Gandon/phages/results/blast_protospacers_30/blast_proto30-genome.tab'
blast_file = sys.argv[1]
# vcf_file='/mnt/alpha_raid/work/Gandon/phages/test_freebayes/results/TO-WT_S83_phag.vcf'

vcf_file='/mnt/alpha_raid/work/Gandon/coevolution/phages/results/snpcalling/W_seq/W1T1_S18.vcf'


## Input data from blast outfmt 6

with open(blast_file, 'r')  as sf :
	lines = sf.readlines()

coordinates_list = []

print("Length coordinates list : {0}".format(len(coordinates_list)))

for i in lines:
	line=i.split('\t')
	coordinates_list.append(line[8:10])   

del(lines, sf, i, line)



## Input data from VCF


with open(vcf_file, 'r')  as sf :
	lines = sf.readlines()

header=[]
vcf_contents=[]
for i in lines:
	if i[0] == '#':
		header.append(i)
	else:
		vcf_contents.append(i)

# for i in vcf_contents:
# 	line=i.split('\t')
# 	print(line[1], '-- i+1')
# 	for j in coordinates_list:
# 		print('---- j+1')
# 		if int(line[1]) in range(int(j[0]), int(j[1])+1):
# 			print('#### FOUND\n', i)
# 		else:
# 			print('@@@@ NOT FOUND: pos:{0}; from:{1}; to:{2}:'.format(line[1], j[0], j[1]))


# Organize The Intervals So That A <= B
coordinates_list_altb = []
for i in coordinates_list:
	if int(i[0]) <= int(i[1]):
		coordinates_list_altb.append(i)
	else:
		coordinates_list_altb.append([i[1],i[0]])


mutation_in_region = []

for i in vcf_contents:
	line=i.split('\t')
	# print(line[1], '-- i+1')
	for j in coordinates_list_altb:
		# print('---- j+1')
		if int(line[1]) in range(int(j[0]), int(j[1])+1):
			print('#### FOUND\n', i)
			mutation_in_region.append(i)

		# else:
			# print('@@@@ NOT FOUND: pos:{0}; from:{1}; to:{2}:'.format(line[1], j[0], j[1]))
