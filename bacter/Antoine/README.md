## Getting new PAMs

From the genome of Streptococcus virus 2972 we'll be looking for new PAMs (Protospacer Associated Motif).

The original scripts were made to teach Antoine Nicot how to program.

The researced patterns are: **AGAA** and **GGAA**; as well as the reverse complementary sequences **TTCT** and **TTCC**.

The PAMS are little sequences of 4 nucleotides located on the 3' side of a protospacer sequence. The lenght of a protospacer taken into account is 32 + 4 (protospacer + PAM).


### Example of sequences & Nomenclature

Here are the templates of sequences.

The PAM sequences are found in 3 prime of the protospacers.
Each needs a unique name of ID for the fasta header.

```
>PAM_Coord_ref_genome Motif=AGAA;Strand=+;Protospacer_length=N
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxAGAA
>PAM_Coord_ref_genome Motif=GGAA;Strand=+;Protospacer_length=N
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxGGAA
>PAM_Coord_ref_genome Motif=AGAA;Strand=-;Protospacer_length=N
TTCTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
>PAM_Coord_ref_genome Motif=GGAA;Strand=-;Protospacer_length=N
TTCCxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

The nomenclature of the fasta header goes as follows:

```
>PAM_152_Sv2972 M=AGAA;Strand=+;Protospacer_length=32
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxAGAA
>PAM_10863_Sv2972 M=TTCT;Strand=-;Protospacer_length=32
TTCTxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

Mandatory fields for ID field in header FASTA. No spaces

* **PAM** = Type of sequence **P**rotospacer **A**ssociated **M**otif
* **152** = The coordinate of the first nucleotide of the motif.
* **Sv2972** = Reference genome used to find these sequences

Comment field for FASTA format. No strict rules. I used Definition=Value without spaces. Each separated by a semi-colon

* **Motif=AGAA** = Motif The motif used to find that sequence
* **Strand=+** = The strand where the sequence is. If the motif is on the strand -, the sequence present will be the reverse complementary.
* **Protospacer_length** = The lenght of sequence concidered to be a protospacer without the lenght of the PAM.
