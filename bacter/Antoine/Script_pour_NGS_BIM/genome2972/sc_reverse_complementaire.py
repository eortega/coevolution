#! /usr/bin/env python

import Bio.SeqIO

sf = 'outputAGAA.txt'
df = 'outputAGAARevComp.txt'


with open(sf, 'r') as f:
    liste = f.readlines()


with open(df, 'w') as d:
    for i in liste:
        d.write(Bio.Seq.reverse_complement(i))


