#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import re


## INPUT FILES AND OTHER INPUTS

# input_code_protospacer = '/home/manager/Documents/analyse_bioinfo/r1/genome2972/protosp_10pbs.txt'
# input_to_replace = '/home/manager/Documents/analyse_bioinfo/r1/Test_total/tout_avec_spacers_en_bases.txt'
# output_file = '/home/manager/Documents/analyse_bioinfo/r1/Test_total/tout_spacers_remplaces.txt'



## ORIGINAL FILES

input_code_protospacer = "proto_sp_13pb_dico"
# input_to_replace = "tout_avec_spacers_en_bases.txt"
# output_file = "spacers_replaced.txt"
# error_output = "spacers_mismatches.txt"


## TEST FILES

input_to_replace = "B0A_en_bases"
output_file = "spacers_replaced.txt"
error_output = "spacers_mismatches.txt"



## IMPORT PROTOSPACER SEQUENCE

with open(input_code_protospacer, 'r') as f:
    code_protospacer = f.readlines()

## DETERMINE LENGTH OF THE PROTOSPACER
key_len = len(code_protospacer[0])-1

## GENERATE 3 DIGIT CODE FROM PROTOSPACER SEQUENCE

dico_code_protospacer = {}

for i in range(0, len(code_protospacer)):
    dico_code_protospacer[ code_protospacer[i][0:key_len] ] = "{0:03d}".format(i + 1)


# print("Code protospacer dico: CREATED")

## IMPORT SEQUENCES TO REPLACE

with open(input_to_replace, 'r') as f:
    to_replace = f.readlines()

# print("input data to replace: DONE")


## CREATE VARIABLES FOR THE LOOP EXECUTION
replaced_list = []

counter1, counter2plus, counter_global = 0, 0, 0

errors_list = []

## LOOK FOR THE PROTOSPACERS AND REPLACE THEM WITH THEIR INDEX NUMBER

for i in to_replace:
    sub_seq = i.split('-')
    # liste_dash = [p.start() for p in re.finditer('-', i)]

    if len(sub_seq) == 2 :

        counter1 += 1
        counter_global += 1
        # replaced_list.append(i)
        replaced_list.append('DGCC\n')


    if len(sub_seq) >= 3:
        counter2plus += 1
        counter_global += 1

        flag = True
        flag_liste = []

        ## LOOP ON THE ELEMENTS OF THE LINE EXCEPT THE FIRST (0) AND LAST (-1)
        for k in range( 1, len(sub_seq) - 1 ):

            ## PARSE DICTIONARY.KEYS AND REPLACED
            # print(i, sub_seq[k])
            if sub_seq[k][0:key_len] in dico_code_protospacer.keys():

                ## REPLACE SEQUENCES K FOR THEIR NUMBER GIVEN IN THE DICTIONARY
                # sub_seq[k] = sub_seq[k].replace(sub_seq[k][0:key_len] , dico_code_protospacer[sub_seq[k][0:key_len]])
                sub_seq[k] = dico_code_protospacer[sub_seq[k][0:key_len]]

                # print(i,sub_seq[k], '\n')

                ## TRANSFORM ALL CAPITALS TO LOWER CASE
                #sub_seq[k] = sub_seq[k].lower()


                ## ADD True FLAG TO flag_liste
                flag_liste.append(flag)


            else:


                # print("Error: NOT IN DICO. Element {0} in line number: {1} : {2}".format(k,counter_global, i))
                # errors_list.append("Error: NOT IN DICO. Element {0} in line number: {1} : {2}".format(k,counter_global, i))


                ## ADD False TO flag_liste
                flag = False
                flag_liste.append(flag)


        # print(sub_seq)

        ## TEST IF THERE ARE ANY False IN THE flag_liste.
        ## ONLY ADD THE LINES WHICH SEQUENCES CONTAILN PROTOSPACERS

        if False in flag_liste:
            errors_list.append("-".join(sub_seq))
        else:
            # replaced_list.append("-".join(sub_seq))
            replaced_list.append("-".join(sub_seq[1:-1])+'\n')

        # print(replaced_list[-1])


print("1 dash {0};\n2+ dash {1}".format\
        (counter1, counter2plus))
print("total lines = {0}".format(counter_global))



## WRITE REPLACED LINES TO FILE

with open(output_file, 'w') as f:
    for i in replaced_list:
        f.write(i)

## WRITE SPACERS NOT FOUND, WITH LINE NUMBER AND SEQUENCES
with open(error_output, 'w') as f:
    for i in errors_list:
        f.write(i)
