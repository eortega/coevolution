#!/bin/bash

echo 'running... ' $(basename $0)

FILE=$1
outdir=$2

awk 'length($0)<5' $FILE > ${outdir}inter5mono
wc -l ${outdir}inter5mono > ${outdir}comptageinter5mono

sort -n ${outdir}inter5mono | uniq -c > ${outdir}out_result_mono
