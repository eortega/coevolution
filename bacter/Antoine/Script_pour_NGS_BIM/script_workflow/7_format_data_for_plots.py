#!/usr/bin/env python

import os, fnmatch, csv
import pandas as pd


class Control:


    def __init__(self, path_to_dir, control_dir_base_name, data_file):
        '''
        path_to_dir = directory containing all the sub directories of the
            output_file
        base_dir_name = Base name of the directories of the controls, excluding
            the different chars i.e.:
            FILES = "control_A.csv", "control_B.csv", "control_C.csv"
            give = "control_"
        '''

        ## PUTING ARGUMENTS IN VARIABLES
        self._path_control = path_to_dir
        self._listdir = os.listdir(self._path_control)
        self._control_root_name = control_dir_base_name + "*"
        self._data_file = data_file

        ## INITIALIZE PUBLIC ATTRIBUTES
        self.sample_list_full_path = []
        self.control_list_full_path = []
        self.sample_list_basename = []
        self.control_list_basename = []
        self.sample_dico_contents = {}
        self.control_dico_contents = {}

        ## INITIALIZE PRIVATE ATTRIBUTES
        self.samples_root_name = []
        self._sample_replicas = {}

        self.wanted_tags = ['014', '077', '170', '182', '198', '208', '263', '274', '282', '317', '323', '331', '363', '390', '401', '404', 'DGCC']


    def list_files(self):
        '''
        Lists of the files to be used with their full paths.
        Each element is:
            path_to_dir + base_dir_name + numerotation + file.txt
        Uses method open_and_import to put all the files into the memory
        '''


        ## LOOP FOR ALL FILES (DIR) FOUND IN A DIRECTORY
        for i in self._listdir :

            # print(i)

            ## THE BASE NAME IS IN A LOCAL VARIABLE
            ## TO BE USED BY OTHER METHODS
            self._directory_basename = i

            ## IF IT'S A CONTROL FILE
            if fnmatch.fnmatch(i, self._control_root_name):

                ## APPEND FULL PATH TO CONTROL FILES TO A LIST
                self.control_list_full_path.append( '/'.join(
                [self._path_control, self._directory_basename, self._data_file] ) )

                ## APPEN DIRS' BASENAME TO A LIST
                self.control_list_basename.append(self._directory_basename)

                ## CREATE DICTIONARY KEY + EMPTY LIST
                self.control_dico_contents[self._directory_basename] = []

                ## OPEN AND IMPORT FILE METHOD
                self.open_and_import2('/'.join(
                [self._path_control, self._directory_basename, self._data_file] ),
                self.control_dico_contents )


            ## IF FILES ARE SAMPLE FILES
            else:

                ## APPEND FULL PATH TO SAMPLE FILES TO A LIST
                self.sample_list_full_path.append( '/'.join(
                [self._path_control, self._directory_basename, self._data_file] ) )

                ## APPEN DIRS' BASENAME TO A LIST
                self.sample_list_basename.append(self._directory_basename)

                ## CREATE DICTIONARY KEY + EMPTY LIST
                self.sample_dico_contents[self._directory_basename] = []

                ## OPEN AND IMPORT FILE METHOD
                self.open_and_import2('/'.join(
                [self._path_control, self._directory_basename, self._data_file] ),
                self.sample_dico_contents )

                ## GET ROOT NAME OF SAMPLE FILES INTO A LIST WITHOUT LAST CHARACTER
                self.get_root_name(self._directory_basename)


## DEPRECATED MOETHOD
    def open_and_import1(self, input_file, dico):
        '''
        Import a file contents, set data type, put contents in a dictionary
        '''
        with open(input_file, 'r') as sf:
            reader = csv.reader(sf, skipinitialspace = True, delimiter = ' ',
            quoting = csv.QUOTE_NONE)

            for row in reader :

                dico[self._directory_basename].append(row)


    def open_and_import2(self, input_file, dico):
        '''
        Import a file contents, set data type, put contents in a dictionary
        Checks that the list would
        '''
        new_dico= {}
        with open(input_file, 'r') as sf:
            reader = csv.reader(sf, skipinitialspace = True, delimiter = ' ',
            quoting = csv.QUOTE_NONE)

            for row in reader :

                new_dico[row[1]] = row[0]

        dico[self._directory_basename] = self.check_wanted_tags(new_dico)




    def check_wanted_tags(self, dico):
        '''
        Parse EACH AND EVERY line to check that there are tags we want.
        In case they are absent add a 0 to that tags
        --> Tags represent certain DNA sequences,\
            the associated number is the number of occurrences in that
            experiment
        '''

        liste_of_lists = []

        for tag in self.wanted_tags :
            try:
                liste_of_lists.append([dico[tag], tag])
            except:
                liste_of_lists.append(['0',tag])

        return liste_of_lists



    def get_root_name(self, input_basename):
        '''
        Get the root name of the files (remove last character),
        add them into a public list.
        If root is already present, don't add it.
        '''
        if input_basename[0:-1] in self.samples_root_name:
            pass
        else:
            self.samples_root_name.append(input_basename[0:-1])
        self.samples_root_name = sorted(self.samples_root_name)



    def calculate_mean_of_control(self):
        '''
        Calculates the mean for all control samples
        '''

        ## CREATE DICTIONARY. TO BE REMOVED FROM SELF. !!
        self.dico = {}

        ## PARSE AN ORDERED LIST OF CONTROL INPUT NAMES
        for i in range(len(self.control_list_basename)):
            new_list = []

            # print(self.control_list_basename[i])

            ## PARSE EACH DATA ENTRY FROM THE DATA DICTIONARY
            ## PUT THE DATA, NOT THE LABELS INTO A NEW LIST
            for j in self.control_dico_contents[self.control_list_basename[i]] :
                # print(j)
                new_list.append(j[0])

            ## ADD THE NEW LIST AS THE VALUE FOR A NEW DICO ENTRY
            ## WHERE THE KEY IS THE CONTROL NAME
            self.dico[a.control_list_basename[i]] = new_list

        ## CONVERT TO DATAFRAME
        self.control_df = pd.DataFrame(self.dico, dtype=int, index=self.wanted_tags)
        #, index=self.wanted_tags)

        ## CALCULATE MEAN
        self.control_df_mean = self.control_df.mean(axis=1)



    def organize_data_per_sample(self, samples_root_name):
        '''
        Parses a sample
        '''
        ## CREATE AN ORDERED LIST OF ALL SAMPLES TO BE TREATED
        self._single_sample_list = []
        for i in a._listdir:
            if fnmatch.fnmatch(i, samples_root_name + '*'):
                self._single_sample_list.append(i)
        self._single_sample_list = sorted(self._single_sample_list)

        dico = {}
        for i in self._single_sample_list:
            new_list = []
            for j in self.sample_dico_contents[i]:
                new_list.append(j[0])
            dico[i] = new_list

        self.sample_df = pd.DataFrame(dico, dtype=int, index=self.wanted_tags)
        #, index=self.wanted_tags)

        # print(self.sample_df)


        ## MERGE control_df_mean with self.sample_df
        self.full_control_sample_data =  pd.concat([pd.DataFrame(self.control_df_mean, columns=['Control mean']), self.sample_df], axis=1)

        ## SAVE FILE
        with open(self._path_control + samples_root_name + '.csv', 'w') as outfile:
            self.full_control_sample_data.to_csv(outfile, sep='\t')




if __name__ == "__main__":

    ## INITIALIZE CLASS
    a = Control('/media/sf_analyse_bioinfo/test_out/', 'Mix-B0-', 'originBIM' )

    a.list_files()

    ## CALCULATE MEAN OF CONTROL AND PUT IT IN ONE LIST
    a.calculate_mean_of_control()


    ## ITERATE INTO ALL THE RESULTS FOLDERS AND ORGANIZE DATA TO BE PLOTTED
    for i in a.samples_root_name:
        a.organize_data_per_sample(i)

