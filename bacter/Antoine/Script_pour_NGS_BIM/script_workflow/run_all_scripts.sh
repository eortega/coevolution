#!/bin/bash

echo 'running... ' $(basename $0)


FILE=$1
outdir=$2
DICO=$3

pwd=$(dirname $0)

mkdir -p $outdir


bash $pwd/1_sc_tout.sh $FILE $outdir

python $pwd/2_replace_protospacer6.py $outdir $DICO

bash $pwd/3_sc_countReplMism.sh $outdir

bash $pwd/4_sc_count_stain_Mono.sh ${outdir}inter4_replaced $outdir

bash $pwd/5_sc_count_stain_MonoMulti.sh ${outdir}inter4_replaced  $outdir

bash $pwd/6_sc_BIMlines.sh ${outdir}out_result_mono $outdir



#mv ${outdir}inter* $outdir
#mv ${outdir}out* $outdir
#mv ${outdir}originBIM $outdir
cat ${outdir}comptag* > ${outdir}${shortname}/bilan_comptage
rm ${outdir}comptag*


