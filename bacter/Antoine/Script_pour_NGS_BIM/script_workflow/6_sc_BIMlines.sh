#!/bin/bash

echo 'running... ' $(basename $0)

FILE=$1
outdir=$2


grep 014 $FILE > ${outdir}014
grep 077 $FILE > ${outdir}077
grep 170 $FILE > ${outdir}170
grep 182 $FILE > ${outdir}182
grep 198 $FILE > ${outdir}198
grep 208 $FILE > ${outdir}208
grep 263 $FILE > ${outdir}263
grep 274 $FILE > ${outdir}274
grep 282 $FILE > ${outdir}282
grep 317 $FILE > ${outdir}317
grep 323 $FILE > ${outdir}323
grep 331 $FILE > ${outdir}331
grep 363 $FILE > ${outdir}363
grep 390 $FILE > ${outdir}390
grep 401 $FILE > ${outdir}401
grep 404 $FILE > ${outdir}404
grep DGCC $FILE > ${outdir}dgcc

cat ${outdir}014 ${outdir}077 ${outdir}170 ${outdir}182 ${outdir}198 ${outdir}208 ${outdir}263 ${outdir}274 ${outdir}282 ${outdir}317 ${outdir}323 ${outdir}331 ${outdir}363 ${outdir}390 ${outdir}401 ${outdir}404 ${outdir}dgcc > ${outdir}originBIM
rm ${outdir}014 ${outdir}077 ${outdir}170 ${outdir}182 ${outdir}198 ${outdir}208 ${outdir}263 ${outdir}274 ${outdir}282 ${outdir}317 ${outdir}323 ${outdir}331 ${outdir}363 ${outdir}390 ${outdir}401 ${outdir}404 ${outdir}dgcc




# grep 014 out_result_mono > 014
# grep 077 out_result_mono > 077
# grep 170 out_result_mono > 170
# grep 182 out_result_mono > 182
# grep 198 out_result_mono > 198
# grep 208 out_result_mono > 208
# grep 263 out_result_mono > 263
# grep 274 out_result_mono > 274
# grep 282 out_result_mono > 282
# grep 317 out_result_mono > 317
# grep 323 out_result_mono > 323
# grep 331 out_result_mono > 331
# grep 363 out_result_mono > 363
# grep 390 out_result_mono > 390
# grep 401 out_result_mono > 401
# grep 404 out_result_mono > 404
# grep DGCC out_result_mono > dgcc
# 
# cat 014 077 170 182 198 208 263 274 282 317 323 331 363 390 401 404 dgcc > originBIM
# rm 014 077 170 182 198 208 263 274 282 317 323 331 363 390 401 404 dgcc

