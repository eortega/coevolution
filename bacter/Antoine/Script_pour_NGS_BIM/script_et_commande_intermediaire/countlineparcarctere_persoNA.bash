#!/bin/bash
FILE=$1
awk '{print length($0);}' $FILE | sort -n | uniq -c;
