#!/bin/bash

FILE=$1
outdir=$2

./1_sc_tout.sh $FILE
./2_replace_protospacer6.py
./3_sc_countReplMism.sh
./4_sc_count_stain_Mono.sh inter4_replaced
./5_sc_count_stain_MonoMulti.sh inter4_replaced
./6_sc_BIMlines

mkdir -p $outdir
mv inter* $outdir
mv out* $outdir
mv originBIM $outdir
cat comptag* > bilan_comptage
mv bilan_comptage $outdir
rm comptag*


