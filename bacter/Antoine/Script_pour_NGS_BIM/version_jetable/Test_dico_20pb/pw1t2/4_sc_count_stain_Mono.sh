#!/bin/bash

FILE=$1
awk 'length($0)<5' $FILE > inter5mono
wc -l inter5mono > comptageinter5mono

sort -n inter5mono | uniq -c > out_result_mono
