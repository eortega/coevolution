#!/bin/bash

FILE=$1
outdir=$2

./1_sc_tout.sh $FILE
./2_replace_protospacer6.py
./3_sc_countReplMism.sh
./4_sc_count_stain_Mono.sh inter4_replaced
./5_sc_count_stain_MonoMulti.sh inter4_replaced

mkdir -p $outdir
mv inter* $outdir
mv out* $outdir
cat comptageinter1 comptageinter2 comptageinter3noreplaced comptageinter4_mismatch comptageinter4_replaced comptageinter5mono > comptage
mv comptage $outdir
rm comptageinter1 comptageinter2 comptageinter3noreplaced comptageinter4_mismatch comptageinter4_replaced comptageinter5mono


