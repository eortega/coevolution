#!/usr/bin/env python3


import re



#source file
seq_test = "/home/manager/Documents/analyse_bioinfo/genome2972/seq_test"
sf = "/home/manager/Documents/analyse_bioinfo/genome2972/Genome2972_1line.txt"


#output destination file
df = "/home/manager/Documents/analyse_bioinfo/genome2972/output.txt"


# RESEARCHED PATTERN
pat = "AGAA"




with open(sf, 'r') as f:
    texte = f.readlines()[0]
    texte = texte.upper()

#indices = [p.start() for p in re.finditer(pat, texte)]

## FENETRE GLISSANTE
indices = []

for i in range(0,len(texte)):
    a = texte[i : i + len(pat)]
    if pat in a:
        indices.append(i)


#print(texte.index("A"))

# with open(df, 'w') as dest:
#     for i in indices:
#         output = texte[i - 32 : i + 4]
#         print(output)
#         dest.write(output + "\n")


with open(df, 'w') as dest:
    for i in indices:
        if i < 32: 
            pass
        else:
            output = texte[i - 32 : i + len(pat)]
            print(output)
            dest.write(output + "\n")


