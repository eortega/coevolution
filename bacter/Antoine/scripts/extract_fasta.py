#!/usr/bin/python3

from Bio import SeqIO
from Bio.Seq import Seq 
from Bio.SeqRecord import SeqRecord 
from Bio.Alphabet import generic_dna 
import pandas as pd

## IMPORT FASTA GENOME (FASTA RECORDS)

### FILE PATH & NAME
fasta_genome = 'data/fake_genome.fasta'

### BLAST OUTPUT PATH & NAME
blast_out = 'this_file'  
bast_df = pd.read(blast_out)

output_file = 'potato.fasta' 

######################################################

### When you import a fasta file with many fasta sequences
### You will have to parse the sequences

### This values come from a the blast_df
### This blast_df must be parsed line by line
### to extract the values to replace the following variables


for line in blast_df: ### Find how to parse a dataframe per line

    ## Example: 
    subject = 'NC_007019.1'
    s_start = 0
    s_end = 10
    
    # subject = blast_df.subject
    # s_start = blast_df.s_start
    # s_end = blast_df.s_end

    
    ## Create an empty list to store the records (fasta "genes" you annotated)
    record_list = []
    
    
    for scaffold in SeqIO.parse(fasta_genome, 'fasta'):
        ## Print scaffold information
        # print(i)
    
        ## If subject from blast is the same as the scaffold name:
        if subject == scaffold.id:
    
            ## Slice a piece of the sequence from the s_start to the s_end of the match
            chunk = scaffold.seq[ s_start -1 : s_end ]
            ## Create a uniq ID for this fasta with the scaffold name, and the start and end coordinates of the match
            id_str = subject + '[' + str(s_start) + ':' + str(s_end) + ']'
            ## Here you want to add the query name, which is the name of the fasta protein, used to annotate
            description_str = 'query name and stuff'
    
            ## Create a new record containing all the sequence, id and description
            new_record = SeqRecord(seq = chunk, id = id_str , description = description_str)
    
            ## Add the new_record into the record_list
            record_list.append(new_record)
    
            ## Get out if this iteration of the loop if the subject == scaffold.id
            break


## Write new records to a file

with open(output_file, 'w') as handle:
    for i in lista:
        SeqIO.write(i, handle, 'fasta')





